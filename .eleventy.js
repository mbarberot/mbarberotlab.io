require('dotenv').config()

const { EleventyRenderPlugin } = require("@11ty/eleventy");
const eleventyTailwindPlugin = require('eleventy-plugin-tailwindcss')
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight")

const registerHelpers = require('./site/src/config/register-helpers')
const compareAsc = require('date-fns/compareAsc')

const tags = require('./site/data/available_tags.json').map(it => it.name)

const collectionByUrlPrefix = (prefix) => {
  return (collectionApi) => {
    return collectionApi
      .getAll()
      .filter(page => page.url.startsWith(prefix))
      .sort((a, b) => compareAsc(a.data.date, b.data.date))
  }
}

module.exports = function (eleventyConfig) {

  eleventyConfig.addPlugin(eleventyTailwindPlugin, {
    src: "site/src/css/*.css",
    dest: "assets/css",
    keepFolderStructure: false,
  })

  eleventyConfig.addPlugin(syntaxHighlight)

  eleventyConfig.addPassthroughCopy({
    "site/src/fonts": "assets/fonts",
    "_notion": "notion",
  })

  eleventyConfig.addPlugin(EleventyRenderPlugin)

  registerHelpers(eleventyConfig)

  eleventyConfig.addCollection("posts", collectionByUrlPrefix("/posts"))
  eleventyConfig.addCollection("projects", collectionByUrlPrefix("/project"))

  for (const tag of tags) {
    eleventyConfig.addCollection(tag, function (collectionApi) {
      return collectionApi.getFilteredByTag(tag)
    })
  }

  eleventyConfig.setTemplateFormats(['md', 'njk', 'png', 'jpg'])

  return {
    dir: {
      input: 'site',
      data: 'data',
      includes: 'src/templates',
    }
  }
}
