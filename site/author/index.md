---
layout: post
title: L'auteur
date: 2021-07-19
---

Je pratique le TDD, j'apprécie le pair programming, les méthodes agiles, je pense que l'intégration continue est
incontournable et j'aime automatiser tout ce qui peut l'être.  
Toujours à l'affut des nouveautés dans mon domaine, j'aime faire de la veille technologique, mettre en pratique mes
découvertes et transmettre mon savoir-faire.  

&nbsp;

Bien que j'aie déjà de l'expérience avec certains langages ou frameworks, je ne me limite pas à ceux-ci. Au contraire
j'aime apprendre et voir de nouvelles technologies.

&nbsp;

----

&nbsp;

Vous pouvez me retrouver ou me contacter sur [LinkedIn](https://www.linkedin.com/in/mathieu-barberot-4b58105a/) ou sur
[Twitter](https://twitter.com/mbarberot) et vous pouvez voir tous mes projets sur [Gitlab](https://gitlab.com/mbarberot)
et sur [Github](https://github.com/mbarberot).
