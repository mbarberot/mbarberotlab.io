module.exports = [
    {
        "label": "catégories",
        "link": "/tags",
        "icon": "🏷️",
    },
    {
        "label": "projets",
        "link": "/projects",
        "icon": "🚀",
    },
    {
        "label": "rss",
        "link": "/feed.xml",
        "icon": "📰",
        "new_window": true,
    },
    {
        "label": "à propos",
        "link": "/author",
        "icon": "👤",
    }
]
