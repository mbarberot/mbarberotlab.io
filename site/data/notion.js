const { mkdir, writeFile } = require("fs/promises")
const fetch = require("@11ty/eleventy-fetch")

module.exports = async function () {
  /* const json = [
    {
      "id": "azerty",
      "metadata": {
        "title": "Lorem ipsum",
        "date": "2022-04-11",
        "tags": [
          "dev",
          "tips"
        ]
      },
      "content": "# Header 1\n## Header 2\n### Header 3\n\n> quote\n\n```java\npublic class SomeSourceCode {\n\n}\n```\n\nA simple paragraph\n![Cat !](/notion/azerty/cat.png)",
      "images": [{
        "filename": "cat.png",
        "url": "https://images.unsplash.com/photo-1573865526739-10659fec78a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8Y2F0fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
      }]
    }
  ] */

  try {
    const json = await fetch(process.env.BLOG_API_URL, {
      // duration: 1d,
      type: "json",
      fetchOptions: {
        headers: {
          "x_api_token": process.env.BLOG_API_TOKEN,
        },
      },
    })

    await downloadImages(json, "_notion")
    return json

  } catch (e) {

    console.error("Failed to call the blog API", e)
    return []
  }
}

async function downloadImages(json, basePath) {
  return Promise.all(
    json.map(async ({ id, images }) => {
      const imageDir = await createImageDir(id, basePath)
      return fetchImages(imageDir, images)
    })
  )
}

async function createImageDir(postId, basePath) {
  const path = `${basePath}/${postId}`
  await mkdir(path, { recursive: true })
  return path
}

async function fetchImages(basePath, images) {
  return Promise.all(
    images
      .map(({ filename, url }) => ({ filename: `${basePath}/${filename}`, url }))
      .map(fetchImage)
  )
}

async function fetchImage({ filename, url }) {
  const buffer = await fetch(url, { type: "buffer" })
  return writeFile(filename, buffer)
}
