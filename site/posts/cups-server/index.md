---
layout: post
title: Serveur d'impression
date: 2020-08-17
tags:
  - docker
  - raspberry
---

> Cet article fait suite à celui de l'installation sur un Raspberry Pi d'une distribution Raspbian Lite (sans UI) et de Docker. Vous pouvez retrouver cet article [ici](/posts/install-raspberry-pi-with-docker).

Ici encore, on ne fait pas dans l'original. Vous trouverez facilement bon nombre de tutoriels pour transformer un Raspberry Pi en serveur d'impression. Mais là encore, on va le faire avec un conteneur.

Matériel
--------

- un Raspberry Pi avec Docker
- une imprimante et son câble USB

1/ Connecter l'imprimante
-------------------------

Brancher l'imprimante au Pi via son câble USB et allumer l'imprimante.
Normalement elle sera détectée automatiquement par l'OS.

2/ Installer CUPS
-----------------

CUPS est un serveur d'impression. Il est trés répandu et est compatible avec tous les OS.
Mon but étant de garder le système aussi minimal que possible, on va s'éviter les installations et configurations de CUPS en passant par Docker.

Voici la commande que j'ai utilisé :

```bash
docker run \
    --name cups \
    -d \
    -it \
    -p 631:631 \
    -v /var/run/dbus:/var/run/dbus \
    -v /dev/bus/usb:/dev/bus/usb \
    -e ADMIN_PASSWORD=MyCUPSP@ssw0rd \
    --privileged \
    --net=host \
    --restart=unless-stopped \
    ydkn/cups:latest
```
A noter :
- `--net=host` permet au conteneur de se comporter au niveau réseau comme s'il était directement sur la machine hôte. C'est nécessaire car sans cela, l'interface d'administration ne serait disponible que depuis le Raspberry Pi et pas depuis un autre PC du réseau.
- `-v /var/run/dbus:...` et `-v /dev/bus/usb:...` permettent l'accès à l'imprimante conectée en USB depuis le conteneur.

Pour le reste, je vous laisse aller regarder la documentation de [Docker](https://docs.docker.com/) et de [l'image CUPS](https://hub.docker.com/r/ydkn/cups).

3/ Ajouter l'imprimante dans CUPS
---------------------------------

Une fois le conteneur lancé, on peut aller sur l'interface de CUPS, à savoir : l'ip de votre Pi (`ip addr show`) sur le port 631. Chez moi cela donne `http://192.168.1.150:631`.

Depuis cette interface, on va pouvoir aller dans la section "Administration", puis "Trouver de nouvelles imprimantes".

![Interface d'administation de CUPS](/posts/cups-server/images/cups-config-admin.jpg)

CUPS nous demande de s'authentifier et on arrive sur la page suivante pour choisir l'imprimante.
CUPS a détecté mon imprimante, je peux directement l'ajouter.

![Choisir l'imprimante](/posts/cups-server/images/cups-config-choix-imprimante.jpg)

Ensuite, CUPS nous demande quelques informations pour décrire l'imprimante :

![Description de l'imprimante](/posts/cups-server/images/cups-config-description-imprimante.jpg)

On choisit ensuite la marque de l'imprimante :

![Choix de la marque de l'imprimante](/posts/cups-server/images/cups-config-choix-marque.jpg)

Puis le modèle :

![Choix du modèle de l'imprimante](/posts/cups-server/images/cups-config-choix-modele.jpg)

Vous pourrez ensuite ajouter de options d'impression ou prendre la configration par défaut.

Pour vérifier si l'imprimante est bien fonctionnelle, vous pouvez aller dans "Imprimantes", choisir l'imprimante que vous venez d'ajouter, puis dans le menu déroulant "Maintenance", choisissez "Imprimez une page de test"

4/ Profit
---------

Lorsque vous ajouterez une nouvelle imprimante réseau sur votre PC, l'OS devrait automatiquement la trouver.

