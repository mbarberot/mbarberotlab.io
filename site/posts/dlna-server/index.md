---
title: Serveur DLNA
layout: post
date: 2020-08-19
tags:
  - docker
  - raspberry
---

> Cet article fait suite à celui de la mise en place de Samba sur un Raspberry Pi avec Docker. Vous pouvez retrouver cet article [ici](/posts/samba-server).

Disposant d'une TV connectée, j'ai rapidement découvert qu'elle pouvait lire des vidéos via le réseau. Pour cela, elle utilise le standard DLNA. On va donc voir ici comment mettre en place un serveur compatible DLNA sur le Raspberry Pi. Et comme pour les précédents articles, on va utiliser Docker !

C'est quoi un serveur DLNA
--------------------------

> DLNA définit un standard d'interopérabilité permettant la lecture, le partage et le contrôle d'appareils multimédia indépendamment de leur marque ou de leur nature. (source : [Wikipedia](https://fr.wikipedia.org/wiki/Digital_Living_Network_Alliance))

On va utiliser ici le serveur [MiniDLNA](https://doc.ubuntu-fr.org/minidlna) pour sa simplicité : _[make each program do one thing well](https://en.wikipedia.org/wiki/Unix_philosophy)_.


Materiel
--------

- un Raspberry Pi avec Docker
- un disque dur externe en USB

1/ Pré-requis
-------------

Le disque dur est branché au Pi et on dispose d'un point de montage.
Si besoin, la marche à suivre est indiqué dans l'article mentionné ci-dessus.


2/ Installer le serveur MiniDLNA
--------------------------------

Voici la commande que j'ai utilisé :

```bash
docker run \
    --name dlna \
    -d \
    -it \
    --net=host \
    --user=1000 \
    --restart=unless-stopped \
    -v /media/share/videos:/media/videos \
    -e MINIDLNA_MEDIA_DIR=/media/videos \
    -e MINIDLNA_FRIENDLY_NAME=MyDLNA \
    vladgh/minidlna
```

- `--user=1000` permet de lancer le conteneur en utilisant l'utilisateur ayant l'uid `1000`, ce user est celui qui dispose des droits sur le point de montage `/media/share`. On peut trouver l'uid d'un utilisateur en inspectant le fichier `/etc/passwd`.
- `-v /media/share/videos:...` permet de monter une partie du disque dur dans le conteneur. Ensuite, on utilise `-e MINIDLNA_MEDIA_DIR=/media/videos` pour indiquer à MiniDLNA quel dossier utiliser.
- `-e MINIDLNA_FRIENDLY_NAME=...` permet de définir le nom qui apparaitra sur le réseau.

Pour les autres options, je vous laisse regarder la documentation de [Docker](https://docs.docker.com/) et de l'image [vladgh/minidlna](https://github.com/vladgh/docker_base_images/tree/master/minidlna).

3/ Vérification
---------------

Vous pouvez aller inspecter les logs pour vérifier que le démarrage du serveur s'est bien déroulé.
Pour cela, utilisez la commande suivante :
```bash
docker logs dlna
 ```

Dans ces logs, vous devriez voir apparaitre un section indiquant sur le serveur a bien pris en compte les dossiers que vous lui avez indiqué :
```bash
Scanning /media/videos
Scanning /media/videos finished (235 files)!
```
Et une section indiquant que le serveur est bien démarré :
```bash
Starting MiniDLNA version 1.2.1.
HTTP listening on port 8200
```

4/ Profit
---------

Votre TV connectée devrait automatiquement découvrir le serveur et pourra lire les fichiers vidéos (ou audio) que vous aurez mis à disposition.

