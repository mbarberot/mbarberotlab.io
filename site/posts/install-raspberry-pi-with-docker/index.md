---
title: Installer un Raspberry Pi avec Docker
date: 2019-04-08
layout: post
tags:
  - docker
  - raspberry
---

Après avoir bien pris la poussière, j'ai enfin décidé de (re)faire quelque chose de mon Raspberry Pi.
A l'origine, le Pi a été développé pour libérer le pouvoir créatif, mais j'ai toujours été rebuté par la maintenance d'un serveur Linux et le bazar que générait l'installation des bases de données, serveurs et autres outils.

Heureusement, maintenant nous avons les conteneurs ! Ils sont parfait pour expérimenter tout un tas de choses sans polluer la machine.
J'ai donc choisi d'utiliser une distribution *Raspbian Lite* sans interface graphique, sur laquelle je vais installer Docker. Mon but est d'avoir un OS aussi dépouillé que possible de n'installer que des conteneurs contenant les applicatifs désirés.

Sources
-------

Pour cette installation, je me suis aidé de plusieurs articles :
- [Raspberry PI Headless Setup](https://linuxhandbook.com/raspberry-pi-headless-setup)
- [Get Started with Docker on Raspberry PI](https://blog.alexellis.io/getting-started-with-docker-on-raspberry-pi)

Matériel
--------

 - un Raspberry Pi (version 2, B+)
 - une carte mémoire (SD Card)
 - de l'équipement réseau (câble RJ45, switch)
 - un autre ordinateur sous Linux (Mint)

1/ Préparation de la carte mémoire
----------------------------------

### Formattage

Tout d'abord on va préparer la carte, et il faut commencer par la formatter.
Pour cela, on insère la carte dans le lecteur. Si comme moi, vous n'en avez pas un d'intégré à votre PC, un adapteur USB fera très bien l'affaire.
Puis, j'ai utilisé l'utilitaire graphique *Disks* de Linux Mint pour trouver la carte parmis les supports de stockage et ensuite pour la formatter via le menu.

Notez bien chemin de la carte mémoire, ça sera utile plus tard. Pour moi, il s'agissait de `/dev/sdc`.

### Téléchargement de Raspbian

Rien de sorcier, voici la page des téléchargements : [Raspberry Pi Downloads](https://www.raspberrypi.org/downloads/raspbian).

### Installation de Raspbian

Pour installer Raspbian, il suffit de copier le contenu de l'archive téléchargée sur la carte mémoire. Pour cela, j'ai utilisé l'utilitaire `dd`, qui prend en paramètre, entre autre le chemin de ma carte mémoire que j'avais pris le soin de noter auparavant.

```bash
unzip -p 2018-06-27-raspbian-stretch-lite.zip
    | sudo dd of=/dev/sdc conv=fsyncp
```

### Un peu de configuration

Par défaut, SSH sera désactivé et ce sera facheux lorsqu'on voudra prendre la main à distance sur le Pi. Pour l'activer, il suffit de créer un fichier vide dans la partiion de *boot* sur la carte mémoire.

```bash
sudo mount /dev/sdc1 /mnt -o umask=000
touch /mnt/ssh
```

2/ Lancement !
--------------

La carte mémoire est prête. Il est temps d'insérer la carte, brancher le Pi au réseau et l'allumer.
Une fois démarré, j'ai tiré parti de ma box internet (Livebox) pour assigner une IP statique et un nom (`backupserver`) au Pi depuis l'interface d'administration, puis je l'ai redémarré pour qu'il prenne en compte les changements.

La configuration réseau terminée, on peut se connecter en SSH en utilisant les identifiants par défaut : `pi` / `raspberry`
```bash
ssh pi@backupserver
```

Pour des raisons évidentes, on va en profiter pour changer tout de suite le mot de passe :
```bash
passwd
```

3/ Installation de Docker
-------------------------

Docker supporte désormais les architectures ARM. On va donc pouvoir l'installer assez facilement sur le Pi !

### Preconfiguration

Avant de pouvoir installer Docker, un peu de configuration est nécessaire.
Tout d'abord, il faut définir le *hostname* dans les fichiers suivants :
 - `/etc/hosts`
 - `/etc/hostname`

Ensuite, on va modifier le fichier `/boot/config.txt` pour modifier ou ajouter la ligne suivante :
```bash
gpu_mem=16
```

### Installer Docker

Docker s'installe très facilement via la commande proposée sur le site officiel :
```bash
curl -sSL https://get.docker.com | sh
```

### Configurer Docker

Dans mon cas, j'ai envie que Docker se lance toujours au démarrage du Pi, c'est pourquoi je configure *systemd* :
```bash
sudo systemctl enable docker
```

Et pour des raisons pratiques, on peut également configurer notre utilisateur pour lui permettre d'utiliser Docker sans avoir systématiquement besoin de *sudo* :
```bash
sudo usermod -aG docker pi
```

On peut maintenant redémarrer le Pi une nouvelle (et dernière) fois.

Profit
------

Le Raspberry Pi est désormais prêt à recevoir des conteneurs !

