---
layout: post
title: "Serveur de backup : Samba"
date: 2019-04-09
tags:
  - backup
  - docker
  - raspberry
---

> Cet article fait suite à celui de l'installation sur un Raspberry Pi d'une distribution Raspbian Lite (sans UI) et de Docker. Vous pouvez retrouver cet article [ici](/posts/install-raspberry-pi-with-docker).

Sans grande originalité, j'ai donc décidé de transformer mon Pi en serveur de fichier pour faire mes sauvegardes et accéder à mes données depuis n'importe quelle PC du réseau local. Le "challenge" sera de le faire uniquement avec des conteneurs !

Dans cette première partie, nous allons voir comment faire la partie partage avec Samba.

Matériel
--------

- un Raspberry Pi avec Docker
- un disque dur externe en USB

1/ Connecter le disque dur externe
----------------------------------

Tout d'abord on va brancher le disque en USB sur le Pi. Etant formatté en NTFS pour pouvoir l'utiliser avec Windows, le Pi est capable de le monter nativement mais uniquement en lecture seule.

Pour pouvoir déposer des fichiers sur le disque dur, on va devoir le monter différement. Pour cela nous allons avoir besoin du paquet `ntfs-3g` :
```bash
sudo apt-get install ntfs-3g
```

Ensuite, on va devoir trouver le disque dur. Pour cela, je regarde habituellement dans `/dev/disk`. Dans mon cas, le disque dur était `/dev/dsb`.
Je peux alors le monter via la commande :
```bash
sudo mount -o rw,uid=1000,gid=1000,umask=0077 \
    -t ntfs-3g \
    /dev/sdb /media/share
```
Notes :
- `-t ntfs-3g` indique le format de la partition, ici nous sommes en NTFS avec le paquet installé précédemment
- `-o` permet de donner des options
  - `uid=1000,gid=1000` indique quel utilisateur aura accès au disque dur. On peut trouver les `uid` et `gid` dans le fichier `/etc/passwd`.
  - `umask=0077` est un *chmod* inversé. Ici, je donne un accès en lecture et écriture.

2/ Lancer Samba avec Docker
---------------------------

Pas besoin de réinventer la roue, un conteneur Samba existe déjà sur Docker Hub : [dperson/samba](https://github.com/dperson/samba).
Le conteneur dispose d'un script permettant de facilement configurer les utilisateurs et partages.

Voici la commande que j'ai utilisé :
```bash
docker run \
	--name samba \
	-d \
	--rm -it \
	-v /media/share:/mnt/share \
	--network host \
	dperson/samba:armhf \
	-n \
	-g "interfaces = 192.168.1.150" \
	-u "myuser;mypassword" \
	-s "Share;/mnt/share;yes;no;no;myuser;myuser" \
	-w "workgroup"
```
Je vous laisse regarder la [documentation](https://github.com/dperson/samba/blob/master/README.md) qui explique bien ce que font les options `-u`, `-s` et `-w` pour me concentrer plutôt sur les autres options.

D'une part pour Docker :
  - `--network host` permet au conteneur de se comporter au niveau réseau comme s'il était directement sur la machine hôte

D'autre par pour le conteneur :
  - `-n` pour démarrer NMDB qui permet de publier les partages, sans quoi le partage n'était pas visible sur les PC de mon réseau
  - `-g "interfaces = <pi's IP>"` qui indique quelle IP doit utiliser NMDB sans quoi il utilise une interface interne au conteneur

3/ Améliorations
----------------

### Monter automatiquement le disque dur au démarrage du Pi

Comme nous avons fait le montage en ligne de commande, il sera perdu au prochain redémarrage. Pour éviter cela, nous allons l'ajouter dans `/etc/fstab`.

Tout d'abord, il faut trouver l'UID du disque dur :
```bash
$ sudo blkid
# [...]
/dev/sdb : LABEL="MyBackup" UUID="1E3BEA7C1AEA5087" TYPE="ntfs" PARTUUID="00015c12-01"
# [...]
```

Ensuite, on peut modifier `/etc/fstab` pour ajouter une ligne contenant les mêmes informations que la commande de *mount*.
```bash
# <uid> <mount point> <format> <options> <dump> <pass>
UUID="1E3BEA7C1AEA5087" /media/share	ntfs-3g	uid=1000,guid=1000,umask=0077	0	0
```

### Lancer automatiquement Samba au démarrage du Pi

Dans la commande Docker, remplacer `-rm` par `--restart=always`.

Profit
------

Le partage Samba devrait être accessible depuis la section "Réseau" de l'explorateur de fichier.
Si ce n'est pas le cas, vous pouvez également utiliser le chemin suivant : `smb://192.168.1.150`

