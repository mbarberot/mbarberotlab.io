---
title: Serveur de numérisation
date: 2020-08-19
layout: post
tags:
  - docker
  - raspberry
---

> Cet article fait suite à celui de l'installation sur un Raspberry Pi d'une distribution Raspbian Lite (sans UI) et de Docker. Vous pouvez retrouver cet article [ici](/posts/install-raspberry-pi-with-docker).

Après avoir installé un serveur d'impression dans [un article précédent](/posts/cups-server), nous allons maintenant voir comment également mettre le scanner à disposition sur le réseau, et là encore avec Docker pour minimiser le nombre d'action à réaliser sur le Pi.

Sources
-------

Pour cette installation, je me suis aidé de plusieurs articles :

- [Raspberry Pi Print & Scanner Server](https://samhobbs.co.uk/2014/07/raspberry-pi-print-scanner-server)
- [Scanner à distance avec le Raspberry Pi](https://www.framboise314.fr/scanner-a-distance-avec-le-raspberry-pi/)

Matériel
--------

- un Rapsberry Pi avec Docker
- une imprimante et son câble USB

1/ Connecter le scanner
-----------------------

Brancher le scanner au Pi via son câble USB et allumer le scanner.
Normalement il sera automatiquement détecté

Dans mon cas, je dispose d'une imprimante/scanner tout-en-un. L'appareil est déjà branché au Pi depuis l'installation du serveur d'impression.


2/ Installer Sane
-----------------

[Sane](http://www.sane-project.org/) est un serveur de numérisation. Il dispose de plusieurs clients pour être utilisable sur les différents OS existants.
Hélas, peu d'images existent pour Raspberry Pi, on va donc créer notre propre image.

Pour cela, allez sur [mbarberot/docker-saned](https://gitlab.com/mbarberot/docker-saned) et suivez les instructions du README pour créer l'image.

Ensuite, voici la commande que j'ai utilisé :
```bash
docker run \
    -d \
    -it \
    --name sane \
    -v /var/run/dbus:/var/run/dbus \
    -v /dev/bus/usb:/dev/bus/usb \
    -p 10000:10000 \
    -p 10001:10001 \
    -p 6566:6566 \
    --privileged \
    --net=host \
    --restart=unless-stopped \
    sane:latest
```

- `-v /var/run/dbus:...` et `-v /dev/bus/usb:...` permettent l'accès à l'imprimante conectée en USB depuis le conteneur

Pour le reste, je vous laisse aller regarder la documentation de [Docker](https://docs.docker.com/).

3/ Profit
---------

Le scanner devrait être accessible depuis le réseau.
Le site de [Sane](http://www.sane-project.org/sane-frontends.html) dispose d'une page référençant les différents clients.

Pour ma part, j'ai opté pour :
- [Simple Scan](https://doc.ubuntu-fr.org/simple-scan) sous Linux, il s'agit de l'utilitaire installé par défaut sous Linux Mint et il fonctionne parfaitement avec Sane.
- [SaneTwain](https://sanetwain.ozuzo.net/) sous Windows, c'est rustique mais ça fonctionne bien.



