const debugHelper = require("../helpers/debug")
const getHelper = require("../helpers/get")
const taglistHelper = require("../helpers/taglist")
const eqHelper = require("../helpers/eq")
const formatDateHelper = require("../helpers/format-date")
const formatDateISOHelper = require("../helpers/format-date-iso")
const reverseArrayHelper = require("../helpers/reverse-array")
const lastUpdateDateHelper = require("../helpers/last-update-date")


module.exports = function(eleventyConfig) {

  // eleventyConfig.addHandlebarsHelper("eq", eqHelper)
  // eleventyConfig.addHandlebarsHelper("formatDate", formatDateHelper)
  // eleventyConfig.addHandlebarsHelper("taglist", taglistHelper)
  // eleventyConfig.addHandlebarsHelper("reverse", reverseArrayHelper)
  // eleventyConfig.addHandlebarsHelper("get", getHelper)

  eleventyConfig.addShortcode("debug", debugHelper)
  eleventyConfig.addShortcode("formatDate", formatDateHelper);
  eleventyConfig.addShortcode("formatDateISO", formatDateISOHelper)
  eleventyConfig.addNunjucksGlobal("lastUpdateDate", lastUpdateDateHelper)
}
