const formatDateHelper = require("./format-date")

module.exports = function (date) {
  return formatDateHelper(date, "yyyy-MM-dd'T'hh:mm:ss'Z'")
}
