const formatDate = require("date-fns/format")

module.exports = function (date, format = "yyyy-MM-dd") {
  return formatDate(date, format)
}
