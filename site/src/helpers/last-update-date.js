const reverseArray = require('./reverse-array')

module.exports = function(posts) {
  if (posts.size <= 0) {
    return null;
  }

  const lastUpdatedPost = reverseArray(posts)[0]
  return lastUpdatedPost.date;
}
