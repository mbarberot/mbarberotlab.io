module.exports = function (tags) {
  if (tags) {
    return tags.map(tag => `#${tag}`).join(", ")
  }

  return []
}
