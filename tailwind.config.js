module.exports = {
  purge: [
    './site/**/*.hbs',
    './site/**/*.css',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      handwriting: ['"Indie Flower"', 'serif'],
      sans: ['Raleway', 'sans'],
      mono: ['"JetBrains Mono"', 'monospace'],
      title: ['Montserrat', 'serif'],
    },
    screens: {
      'sm': '400px',
      'md': '700px',
    },
    extend: {},
  },
  variants: {},
  plugins: [],
}
